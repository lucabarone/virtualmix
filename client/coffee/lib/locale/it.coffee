# Schema di traduzione code -> italian
define [], ->

  phrases =
    app:
      'send':   'invia'
      'new':    'nuovo'
      'create': 'crea'
      'add':    'aggiungi'

    userPanel:
      'profile': 'profilo'
      'login':   'login'
      'logout':  'logout'

    mainPanel:
      'archive': 'archivio'

    home:
      'welcome to': 'benvenuto in'

    archive:
      'aggregates':          'aggregati'
      'aggregate':           'aggregato'
      'cements':             'cementi'
      'cement':              'cemento'
      'additives':           'additivi'
      'additive':            'additivo'
      'additions':           'aggiunte'
      'addition':            'aggiunta'
      'quarries':            'cave'
      'quarry':              'cava'
      'supplier':            'fornitore'
      'suppliers':           'fornitori'
      'manufacturer':        'produttore'
      'manufacturers':       'produttori'
      'product':             'prodotto'
      'suppliers management': 'gestione fornitori'

    suppliers:
      'name':             'ragione sociale'
      'postal_code':      'CAP'
      'address':          'indirizzo'
      'city':             'città'
      'region':           'regione'
      'type':             'tipo'
      'aggregate':        'aggregato'
      'cement':           'cemento'
      'additive':         'additivo'
      'addition':         'aggiunta'
      'data_table_title': 'archivio dei fornitori'

    aggregates:
      'name':             'nome commerciale'
      'code':             'codice'
      'sample_weight':    'peso campione'
      'pa':               'massa volumica apparente dei granuli'
      'prd':              'massa volumica dei granuli preessiccati in stufa'
      'pssd':             'massa volumica dei granuli saturi con superficie asciutta'
      'wa24':             "assorbimento d'acqua dopo immersione per 24h"
      'origin':           'provenienza'
      'cost':             'costo'
      'natural':          'naturale'
      'crushed':          'frantumazione'
      'grading_analysis': 'analisi granulometrica'
      'analysis_date':    'data'
      'manufacturer':     'produttore'
      'supplier':         'fornitore'
      'datatable_title':  'archivio degli aggregati'
      'author':           'autore'

    sieves:
      'series': 'serie'
      'opening': 'diametro'

    additives:
      'additive':        'additivo'
      'category':        'categoria'
      'manufacturer':    'produttore'
      'name':            'nome commerciale'
      'supplier':        'fornitore'
      'cost':            'costo'
      'datatable_title': 'archivio degli additivi'

    additions:
      'addition':        'aggiunta'
      'code':            'codice'
      'name':            'nome commerciale'
      'manufacturer':    'produttore'
      'supplier':        'fornitore'
      'type':            'tipo'
      'cost':            'costo'
      'silica_fume':     'fumo di silice'
      'calcareous':      'calcareo'
      'fly_ash':         'cenere volante'
      'datatable_title': 'archivio delle aggiunte'

    cements:
      'cement':          'cemento'
      'code':            'codice'
      'type':            'tipo'
      'cost':            'costo'
      'manufacturer':    'produttore'
      'supplier':        'fornitore'
      'datatable_title': 'archivio dei cementi'

    users:
      'name':            'nome'
      'surname':         'cognome'
      'type':            'tipo'
      'role':            'ruolo'
      'pro':             'professionista'
      'company':         'azienda'
      'stud':            'studente'
      'dev':             'sviluppatore'
      'admin':           'amministratore'
      'mono':            'monolicenza'
      'multi':           'multilicenza'
      'datatable title': 'archivio degli utenti registrati'

    grading_analysis:
      'date':          'data prova'
      'series':        'serie'
      'sieves series': 'serie setacci'
      'opening':       'diametro [mm]'
      'imr':           'trattenuto [g]'
      'ipr':           'trattenuto [%]'
      'cpr':           'trattenuto cumulativo [%]'
      'cpp':           'passante cumulativo [%]'
      'authorKey':     'prova eseguita da'

    aggregateView:
      'backButton': 'archivio aggregati'
