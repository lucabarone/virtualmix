###
# Classe dedicata all'internazionalizzazione dell'applicazione, dovrebbe
# comprire le seguenti funzionalità:
#   + Traduzione
#   + Traduzione con interpolazione
#   + Traduzione delle forme plurali
###
define [
  'lib/locale/it'
], (it) ->

  class Lang

    phrases: undefined
    locale:  undefined

    constructor: (locale) ->
      @locale = locale

      switch @locale
        when 'en'
          @phrases = en
        else
          @phrases = it

    translate: (subject, key, options) ->
      if @phrases[subject][key]?
        if options?
          @phrases[subject][key](options)
        else
          @phrases[subject][key]
      else
        key

  locale = localStorage.getItem('locale') or 'it'

  return new Lang(locale)
