# Vista di utilità dedicata alle tabell
define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'text!templates/dataTable/main.hbs'
  'text!templates/dataTable/row.hbs'
  'lib/lang'
], ($, _, Backbone, Handlebars, mainTemplate, rowTemplate, lang) ->

  class DataTable extends Backbone.View
    className: 'data-table'

    initialize: (router, collection, options = {}) ->

      if router instanceof Backbone.Router
        @router = router
      else
        console.error 'router parameter required!', router

      if collection instanceof Backbone.Collection
        @collection = collection
        @subject    = (@collection.url).substring(1)
      else
        console.error 'collection parameter required!', collection

      @format    = if options.format? then options.format else undefined
      @deletable = if options.deletable? then options.deletable else true
      @sortable  = if options.sortable? then options.sortable else false
      @navigable = if options.navigable? then options.navigable else false
      @title     = if options.title? then options.title else false

      @listenTo @collection, 'sort add change remove', @render

    template: Handlebars.compile mainTemplate
    render: ->
      if @collection.length > 0
        @columns = []

        unless @format?
          attrs    = _.clone (@collection.at(0)).attributes
          for key, value of attrs
            @columns.push
              code: key
              text: lang.translate @subject, key
        else
          for value in @format
            @columns.push
              code: value.code
              text: lang.translate @subject, value.code

        @$el.html @template
          title:      @title
          columns:    @columns
          operations: @deletable

        tbody = @$el.find 'tbody'
        @collection.forEach (model) =>
          tbody.append (new DataTableRow(model, @format, @deletable)).render().el
      this

    events:
      'click th':                      'columnClicked'
      'click tbody > tr > td.element': 'rowClicked'
      'click tbody > tr input.delete': 'deleteRow'
    columnClicked: (e) ->
      current = $(e.currentTarget)
      if @sortable
        @collection.comparator = current.data 'code'
        @collection.sort()
    rowClicked: (e) ->
      current = $(e.currentTarget).parent().data('id')
      if @navigable
        @router.goto "#{@subject}/#{current}"
    deleteRow: (e) ->
      e.preventDefault();
      res = confirm 'Sei sicuro?'
      if res
        current = $(e.currentTarget)
        element = @collection.get current.closest('tr').data('id')
        console.log 'delete', current.closest('tr').data('id'), element

        element.destroy
          error: (model, res) ->
            console.warn 'DataTable.deleteRow model.destroy error', res


    class DataTableRow extends Backbone.View
      tagName: 'tr'

      initialize: (model, format, deletable) ->
        if model instanceof Backbone.Model
          @model   = model
          @subject = (@model.urlRoot).substring(1)
        else
          console.error 'model parameter required'

        @format = format
        @deletable = deletable

      template: Handlebars.compile rowTemplate
      render: ->
        @attrs =  _.clone @model.attributes
        @values = []
        if @format?
          for value in @format
            @values.push
              code: @attrs[value.code]
              word:
                if value.translate
                  (lang.translate(@subject, @attrs[value.code]))
                else
                  @attrs[value.code]
        else
          for key, value of @attrs
            @values.push
              code: value
              word: value

        @$el.data 'id', @model.get('id')
        @$el.html @template
          values:     @values
          operations: @deletable
          deletable:  @deletable
        this
