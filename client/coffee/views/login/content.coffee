define [
	'jquery'
	'underscore'
	'backbone'
	'handlebars'
	'lib/lang'
	'text!templates/login/content.hbs'
], ($, _, Backbone, Handlebars, lang, contentTemplate) ->

	class Login extends Backbone.View
		className: 'login'

		initialize: (options) ->
			@router = options.router
			@user = options.user
			console.log 'Login.initialize', @user

		render: ->
			template = Handlebars.compile contentTemplate
			@$el.html template
				tlogin: lang.translate 'app', 'login'
				tsend:  lang.translate 'app', 'send'
			return this

		events:
			'keyup input.email': 'vali8email'
			'keyup input.password': 'vali8password'
			submit: 'submit'

		vali8email: (e) ->
			field = $(e.currentTarget)
			regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

			if field.val() is ''
				console.warn 'il campo email non può essere nullo'
				field.addClass 'invalid'
			else
				unless regex.test(field.val())
					console.warn 'il campo email non è valido'
					field.addClass 'invalid'
				else
					console.log 'ok'
					field.removeClass 'invalid'

		vali8password: (e) ->
			field = $(e.currentTarget)

			if field.val() is ''
				console.warn 'il campo password non può essere nullo'
				field.addClass 'invalid'
			else
				console.log 'ok'
				field.removeClass 'invalid'

		submit: (e) ->
			e.preventDefault()
			console.log 'Login.submit'
			fields = @$el.find('ol.fields input')

			unless fields.hasClass('invalid')
				@listenToOnce @user, 'log', =>
					@router.goto ''

				@user.login
					email:    @$el.find('input.email').val()
					password: @$el.find('input.password').val()
