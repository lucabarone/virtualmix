define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'text!templates/mainPanel/content.hbs'
  'lib/lang'
], ($, _, Backbone, Handlebars, contentTemplate, lang) ->

  class MainPanel extends Backbone.View
    tagName:   'ul'
    className: 'menu'

    initialize: (options) ->
      @router = options.router
      @user   = options.user

      @listenTo @user, 'log', =>
        @render()

    template: Handlebars.compile contentTemplate
    render: ->
      @$el.html @template
        logged:  @user.isLogged()
        archive: lang.translate 'mainPanel', 'archive'
      return this

    events:
      'click a.archive': 'archiveClicked'
    archiveClicked: (e) ->
      e.preventDefault()
      @router.goto 'archive'
