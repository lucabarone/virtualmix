define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'text!templates/profile/content.hbs'
  'collections/users'
  'lib/dataTable'
  'lib/lang'
], ($, _, Backbone, Handlebars, contentTemplate, Users, DataTable, lang) ->

  class Profile extends Backbone.View
    className: 'profile'

    initialize: (router, user) ->
      @router = router
      @user = user

    template: Handlebars.compile contentTemplate
    render: ->
      @$el.html @template
        name:      @user.get 'name'
        surname:   @user.get 'surname'
        type:      lang.translate 'users', @user.get('type')
        typeLabel: lang.translate 'users', 'type'

      if @user.get('role') in ['dev', 'admin']
        console.log 'processo di generazione users-table cominciato...'

        @users = new Users
        @users.fetch
          success: (data) =>
            console.log 'users fetch success', data
            @$el.append (new DataTable(@router, @users,
              sortable:  true
              deletable: false
              title:     lang.translate 'users', 'datatable title'
              format: [
                code: 'email'
                translate: false
              ,
                code: 'name'
                translate: false
              ,
                code: 'surname'
                translate: false
              ,
                code: 'role'
                translate: true
              ,
                code: 'type'
                translate: true
              ]
            )).render().el

      return this
