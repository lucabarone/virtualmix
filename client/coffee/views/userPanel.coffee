define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'text!templates/userPanel/content.hbs'
  'lib/lang'
], ($, _, Backbone, Handlebars, contentTemplate, lang) ->

  class UserPanel extends Backbone.View
    tagName:   'ul'
    className: 'menu'

    initialize: (options) ->
      @router = options.router
      @user   = options.user

      @listenTo @user, 'log', =>
        @render()

    render: ->
      template = Handlebars.compile contentTemplate

      @$el.html template
        logged:  @user.isLogged()
        profile: lang.translate 'userPanel', 'profile'
        login:   lang.translate 'userPanel', 'login'
        logout:  lang.translate 'userPanel', 'logout'

      return this

    events:
      'click a.login':   'loginClicked'
      'click a.logout':  'logoutClicked'
      'click a.profile': 'profileClicked'
    loginClicked: (e) ->
      e.preventDefault()
      console.log 'UserPanel.loginClicked'
      @router.goto 'login'
    logoutClicked: (e) ->
      e.preventDefault()
      console.log 'UserPanel.logoutClicked'
      @user.logout()
    profileClicked: (e) ->
      e.preventDefault()
      @router.goto 'profile'
