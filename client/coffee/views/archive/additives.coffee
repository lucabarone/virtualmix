define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'lib/lang'
  'lib/dataTable'
  'text!templates/archive/additives.hbs'
  'models/archive/additive'
  'collections/archive/suppliers'
  'collections/archive/additives'
], (
  $
  _
  Backbone
  Handlebars
  lang
  DataTable
  contentTemplate
  Model
  Suppliers
  Collection
) ->

  class Additives extends Backbone.View
    className: 'additives'

    initialize: (router, user) ->
      @router = router
      @user = user
      @suppliers = new Suppliers
      @collection = new Collection

    template: Handlebars.compile contentTemplate
    render: ->
      @suppliers.fetch
        data: $.param id_user: @user.get('id'), type: 'additive'
        error: (model, res) ->
          console.log 'Suppliers.coll.fetch error', res
        success: (model, res) =>
          console.log 'Suppliers.coll.fetch success', res
          suppliers = new Array
          @suppliers.forEach (supplier) =>
            suppliers.push
              id:   supplier.get('id')
              name: supplier.get('name')

          @$el.html @template
            'send':         lang.translate 'app', 'send'
            'add':          lang.translate 'app', 'add'
            'additive':     lang.translate 'additives', 'additive'
            'category':     lang.translate 'additives', 'category'
            'supplier':     lang.translate 'additives', 'supplier'
            'name':         lang.translate 'additives', 'name'
            'cost':         lang.translate 'additives', 'cost'
            permission:     if @user.get('role') in ['admin', 'dev'] then true else false
            suppliers:      suppliers

          @collection.fetch
            data: $.param(id_user: @user.get('id'))
            error: (coll, res) ->
              console.log 'additives.collection.fetch -> error', res
            success: (coll, res) =>
              console.log 'additives.collection.fetch -> success', res
              @$el.before (new DataTable(@router, @collection,
                title:     lang.translate 'additives', 'datatable_title'
                deletable: false
                sortable:  true
              )).render().el
      return this

    events:
      submit: 'newAdditive'
    newAdditive: (e) ->
      e.preventDefault()

      fields = @$el.find 'ol.fields > li'

      model = new Model
        id_user:     @user.get 'id'
        id_supplier: fields.find('select.supplier').val()
        category:    fields.find('select.category').val()

      @listenToOnce model, 'sync', =>
        @collection.fetch
          data: $.param id_user: @user.get('id')

      model.save
        error: (model, res) ->
          console.log 'additive.save error', res
        success: (model, res) ->
          console.log 'additive.save success', res
