define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'lib/lang'
  'lib/dataTable'
  'text!templates/archive/cements.hbs'
  'models/archive/cement'
  'collections/archive/suppliers'
  'collections/archive/cements'
], (
  $
  _
  Backbone
  Handlebars
  lang
  DataTable
  contentTemplate
  Model
  Suppliers
  Collection
) ->

  class Cements extends Backbone.View
    className: 'cements'

    initialize: (router, user) ->
      @router = router
      @user = user
      @suppliers = new Suppliers
      @collection = new Collection

    template: Handlebars.compile contentTemplate
    render: ->
      @suppliers.fetch
        data: $.param id_user: @user.get('id'), type: 'cement'
        error: (model, res) ->
          console.log 'Suppliers.coll.fetch error', res
        success: (model, res) =>
          console.log 'Suppliers.coll.fetch success', res
          suppliers = new Array
          @suppliers.forEach (supplier) =>
            suppliers.push
              id:   supplier.get('id')
              name: supplier.get('name')

          @$el.html @template
            'add':          lang.translate 'app', 'add'
            'send':         lang.translate 'app', 'send'
            'cement':       lang.translate 'cements', 'cement'
            'code':         lang.translate 'cements', 'code'
            'type':         lang.translate 'cements', 'type'
            'supplier': lang.translate 'cements', 'supplier'
            suppliers:      suppliers

          @collection.fetch
            data: $.param(id_user: @user.get('id'))
            error: (coll, res) ->
              console.log 'cements.collection.fetch -> error', res
            success: (coll, res) =>
              console.log 'cements.collection.fetch -> success', res
              @$el.before (new DataTable(@router, @collection,
                title: lang.translate 'cements', 'datatable_title'
                deletable: false
                sortable:  true
              )).render().el
      return this

    events:
      submit: 'newCement'
    newCement: (e) ->
      e.preventDefault()
      console.log 'newCement'
      fields = @$el.find 'ol.fields > li'

      model = new Model
        id_user:     @user.get 'id'
        id_supplier: fields.find('select.supplier').val()
        code:        fields.find('input.code').val()
        type:        fields.find('select.type').val()

      @listenToOnce model, 'sync', =>
        @collection.fetch
          data: $.param id_user: @user.get('id')

      model.save
        error: (model, res) ->
          console.log 'cement.save error', res
        success: (model, res) ->
          console.log 'cement.save success', res
