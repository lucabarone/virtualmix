define [
	'jquery'
	'underscore'
	'backbone'
	'handlebars'
	'models/archive/supplier'
	'collections/archive/suppliers'
	'lib/lang'
	'lib/dataTable'
	'text!templates/archive/suppliers.hbs'
], (
	$
	_
	Backbone
	Handlebars
	Model
	Collection
	lang
	DataTable
	contentTemplate
) ->

	class Suppliers extends Backbone.View
		className: 'suppliers'

		initialize: (router, userModel) ->
			@router = router
			@user = userModel

		template: Handlebars.compile contentTemplate
		render: ->
			@$el.html @template
				'send':        lang.translate 'app', 'send'
				'create':      lang.translate 'app', 'create'
				'supplier':    lang.translate 'archive', 'supplier'
				'product':     lang.translate 'archive', 'prodotto'
				'name':        lang.translate 'suppliers', 'name'
				'address':     lang.translate 'suppliers', 'address'
				'city':        lang.translate 'suppliers', 'city'
				'region':      lang.translate 'suppliers', 'region'
				'postal_code': lang.translate 'suppliers', 'postal_code'
				'aggregate':   lang.translate 'archive', 'aggregate'
				'cement':      lang.translate 'archive', 'cement'
				'additive':    lang.translate 'archive', 'additive'
				'addition':    lang.translate 'archive', 'addition'

			@collection = new Collection
			@collection.fetch
				data:
					id_user: @user.get('id')
				error: (data) ->
					console.log 'Suppliers.coll.fetch error', data
				success: (data) =>
					console.log 'Suppliers.coll.fetch success', data
					@$el.before (new DataTable(@router, @collection,
						sortable:  true
						deletable: false
						title:     lang.translate 'suppliers', 'data_table_title'
						format: [
							code: 'type'
							translate: true
						,
							code: 'name'
							translate: false
						,
							code: 'city'
							translate: false
						,
							code: 'address'
							translate: false
						,
							code: 'region'
							translate: false
						,
							code: 'postal_code'
							translate: false
						]
					)).render().el

			return this

		events:
			submit: 'newSupplier'
		newSupplier: (e) ->
			e.preventDefault()

			model = new Model
				id_user:     @user.get('id')
				name:        @$el.find('input.name').val()
				address:     @$el.find('input.address').val()
				city:        @$el.find('input.city').val()
				region:      @$el.find('input.region').val()
				postal_code: @$el.find('input.postal_code').val()
				type:        @$el.find('select.product').val()

			@listenToOnce model, 'sync', =>
				console.log 'model sync'
				@collection.fetch
					data: $.param id_user: @user.get('id')
					error: (data) ->
						console.log 'Suppliers.coll.fetch error', data
					success: (data) =>
						console.log 'Suppliers.coll.fetch success', data

			model.save
				error: (model, res) ->
					console.log 'SuppliersView.newSupplier error', res
				success: (model, res) ->
					console.log 'SuppliersView.newSupplier success', res
