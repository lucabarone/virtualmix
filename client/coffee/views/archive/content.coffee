define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'lib/lang'
  'text!templates/archive/content.hbs'
], ($, _, Backbone, Handlebars, lang, contentTemplate) ->

  class Archive extends Backbone.View
    className: 'archive'

    initialize: (options) ->
      @router = options.router

    template: Handlebars.compile contentTemplate
    render: ->
      @$el.html @template
        aggregates:          lang.translate 'archive', 'aggregates'
        cements:             lang.translate 'archive', 'cements'
        additives:           lang.translate 'archive', 'additives'
        additions:           lang.translate 'archive', 'additions'
        suppliers:           lang.translate 'archive', 'suppliers'
        suppliersManagement: lang.translate 'archive', 'suppliers management'
      return this

    events:
      'click a.aggregates': 'aggregatesClicked'
      'click a.cements':    'cementsClicked'
      'click a.additives':  'additivesClicked'
      'click a.additions':  'additionsClicked'
      'click a.suppliers':  'suppliersClicked'
    aggregatesClicked: (e) ->
      e.preventDefault()
      @router.goto 'aggregates'
    cementsClicked: (e) ->
      e.preventDefault()
      @router.goto 'cements'
    additivesClicked: (e) ->
      e.preventDefault()
      @router.goto 'additives'
    additionsClicked: (e) ->
      e.preventDefault()
      @router.goto 'additions'
    suppliersClicked: (e) ->
      e.preventDefault()
      @router.goto 'suppliers'
