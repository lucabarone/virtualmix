define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'lib/lang'
  'lib/dataTable'
  'text!templates/archive/additions.hbs'
  'models/archive/addition'
  'collections/archive/suppliers'
  'collections/archive/additions'
], (
  $
  _
  Backbone
  Handlebars
  lang
  DataTable
  contentTemplate
  Model
  Suppliers
  Collection
) ->

  class Additions extends Backbone.View
    className: 'additions'

    initialize: (router, user) ->
      @router = router
      @user = user
      @suppliers = new Suppliers
      @collection = new Collection

    template: Handlebars.compile contentTemplate
    render: ->
      @suppliers.fetch
        data: $.param id_user: @user.get('id'), type: 'addition'
        error: (model, res) ->
          console.log 'Suppliers.coll.fetch error', res
        success: (model, res) =>
          console.log 'Suppliers.coll.fetch success', res
          suppliers = new Array
          @suppliers.forEach (supplier) =>
            suppliers.push
              id:   supplier.get('id')
              name: supplier.get('name')

          @$el.html @template
            'send':        lang.translate 'app', 'send'
            'add':         lang.translate 'app', 'add'
            'addition':    lang.translate 'additions', 'addition'
            'name':        lang.translate 'additions', 'name'
            'cost':        lang.translate 'additions', 'cost'
            'supplier':    lang.translate 'additions', 'supplier'
            'type':        lang.translate 'additions', 'type'
            'calcareous':  lang.translate 'additions', 'calcareous'
            'fly_ash':     lang.translate 'additions', 'fly_ash'
            'silica_fume': lang.translate 'additions', 'silica_fume'
            suppliers:     suppliers

          @collection.fetch
            data: $.param(id_user: @user.get('id'))
            error: (coll, res) ->
              console.log 'additions.collection.fetch -> error', res
            success: (coll, res) =>
              console.log 'additions.collection.fetch -> success', res
              @$el.before (new DataTable(@router, @collection,
                title:     lang.translate 'additions', 'datatable_title'
                deletable: false
                sortable:  true
              )).render().el
      return this

    events:
      submit: 'newAddition'
    newAddition: (e) ->
      e.preventDefault()

      fields = @$el.find 'ol.fields > li'

      model = new Model
        id_user:     @user.get 'id'
        id_supplier: fields.find('select.supplier').val()
        name:        fields.find('input.name').val()
        type:        fields.find('select.type').val()
        cost:        fields.find('input.cost').val()

      @listenToOnce model, 'sync', =>
        @collection.fetch
          data: $.param id_user: @user.get('id')

      model.save
        error: (model, res) ->
          console.log 'addition.save error', res
        success: (model, res) ->
          console.log 'addition.save success', res
