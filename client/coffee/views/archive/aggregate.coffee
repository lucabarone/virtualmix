define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'text!templates/archive/aggregate.hbs'
  'models/archive/aggregate'
  'models/archive/supplier'
  'collections/archive/sieves'
  'lib/lang'
  'accounting'
], (
  $
  _
  Backbone
  Handlebars
  mainTemplate
  Aggregate
  Supplier
  Sieves
  lang
  accounting
) ->

  class AggregateView extends Backbone.View
    className: 'aggregate'

    initialize: (router, user, id) ->
      @router   = router
      @user     = user
      @model    = new Aggregate id: id
      @supplier = new Supplier id: @model.get('id_supplier')
      @sieves   = new Sieves
      @maths    = new Array
      @points   = new Array

    bootstrap: (options) ->
      @model.fetch
        error: (model, res) ->
          console.warn 'AggregateView model.fetch error', res
        success: (model, res) =>
          console.log 'AggregateView model.fetch success', res

          @model.screenings.fetch
            data: $.param id_aggregate: @model.get('id')
            error: (coll, res) ->
              console.warn 'AggregateView model.screenings.fetch error', res
            success: (coll, res) =>
              console.log 'AggregateView model.screenings.fetch success', res

              @sieves.fetch
                error: (coll, res) ->
                  console.warn 'AggregateView sieves.fetch error', res
                success: (coll, res) =>
                  console.log 'AggregateView sieves.fetch success', res
                  @supplier.fetch()
                  options.success() if options.success?

    template: Handlebars.compile mainTemplate
    render: ->
      @bootstrap
        success: =>
          # Faccio i calcoli per il grafico e le tables
          @sieves = @sieves.filter (sieve) =>
            return sieve.get('series') is @model.get('series')
          @sieves.forEach (sieve) =>
            screening = @model.screenings.where(id_sieve: sieve.get('id'))[0]
            @maths.push
              opening: sieve.get 'opening'
              imr:     parseFloat(accounting.toFixed(screening.get('imr'), 2))
              ipr:     parseFloat(accounting.toFixed(((screening.get('imr') * 100) / @model.get('sample_weight')), 2))
              cpr:     undefined
              cpp:     undefined
          # console.log 'maths', @maths

          # Calcolo il CPR e il CPP
          sum = 0
          for math, i in @maths
            sum = sum + math.ipr
            @maths[i].cpr = parseFloat(accounting.toFixed(sum, 2))
            @maths[i].cpp = parseFloat(accounting.toFixed(100 - sum, 2))
          # console.log 'maths', @maths

          # Mi genero i punti per il grafico
          first = false
          for math in @maths by -1
            unless first
              @points.push
                name: "#{math.opening}mm"
                x:    math.opening
                y:    parseFloat(accounting.toFixed(math.cpp, 2))
              first = true if math.cpp is 100
          # console.log 'points', @points


          # Popolo il template con i dati dell'aggregato in particolare
          @$el.html @template
            operations:
              back: lang.translate 'aggregateView', 'backButton'
            opening: lang.translate 'grading_analysis', 'opening'
            imr:     lang.translate 'grading_analysis', 'imr'
            ipr:     lang.translate 'grading_analysis', 'ipr'
            cpr:     lang.translate 'grading_analysis', 'cpr'
            cpp:     lang.translate 'grading_analysis', 'cpp'
            maths: @maths
            attrs: [
              key: lang.translate 'aggregates', 'name'
              value: @model.get 'name'
            ,
              key: lang.translate 'aggregates', 'code'
              value: @model.get 'code'
            ,
              key: lang.translate 'aggregates', 'sample_weight'
              value: accounting.formatNumber(@model.get('sample_weight'), 2, ' ') + 'g'
            ,
              key: lang.translate 'aggregates', 'cost'
              value: accounting.formatMoney @model.get('cost'), '€'
            ,
              key: lang.translate 'aggregates', 'origin'
              value: lang.translate 'aggregates', @model.get('origin')
            ,
              key: lang.translate 'aggregates', 'pa'
              value: accounting.formatNumber(@model.get('pa'), 2, ' ') + 'g'
            ,
              key: lang.translate 'aggregates', 'prd'
              value: accounting.formatNumber(@model.get('prd'), 2, ' ') + 'g'
            ,
              key: lang.translate 'aggregates', 'pssd'
              value: accounting.formatNumber(@model.get('pssd'), 2, ' ') + 'g'
            ,
              key: lang.translate 'aggregates', 'wa24'
              value: accounting.formatNumber(@model.get('wa24'), 2, ' ') + '%'
            ]
            analysis_date:
              key: lang.translate 'grading_analysis', 'date'
              value: @model.screenings.at(0).get('date')
            series:
              key: lang.translate('grading_analysis', 'sieves series')
              value: @model.get 'series'
            author:
              key: lang.translate 'grading_analysis', 'authorKey'
              value: @model.get 'author'

          @$('#grading_analysis').highcharts
            chart:
              type: 'spline'
            title:
              text: lang.translate 'aggregates', 'grading_analysis'
            tooltip:
              headerFormat: ''
              pointFormat:  '{point.name}: y = {point.y}'
            xAxis:
              title:
                text: "#{lang.translate('sieves', 'opening')} [mm]"
              type:              'logarithmic'
              min:               0.01
              max:               100
              minorTickInterval: 0.1
            yAxis:
              title:
                text: "#{lang.translate('grading_analysis', 'cpp')}"
              min:          0
              max:          100
              tickInterval: 10
            series: [
              name: @model.get 'name'
              data: @points
            ]
      this

    events:
      'click #operations input.back': 'routeBack'
    routeBack: (e) ->
      @router.goto '/aggregates'
