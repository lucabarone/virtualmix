define [
  'jquery'
  'underscore'
  'backbone'
  'handlebars'
  'lib/lang'
  'lib/dataTable'
  'text!templates/archive/aggregates.hbs'
  'text!templates/archive/aggregatesSieves.hbs'
  'models/archive/aggregate'
  'models/archive/screening'
  'collections/archive/aggregates'
  'collections/archive/sieves'
  'collections/archive/suppliers'
], (
  $
  _
  Backbone
  Handlebars
  lang
  DataTable
  contentTemplate
  sievesTemplate
  Model
  Screening
  Collection
  Sieves
  Suppliers
) ->

  class Aggregates extends Backbone.View
    className: 'aggregates'

    initialize: (router, user) ->
      @router = router
      @user = user
      @collection = new Collection
      @sieves = new Sieves
      @suppliers = new Suppliers

    template: Handlebars.compile contentTemplate
    render: ->
      @suppliers.fetch
        data: $.param id_user: @user.get('id'), type: 'aggregate'
        error: (model, res) ->
          console.log 'Manufactures.coll.fetch error', res
        success: (model, res) =>
          console.log 'Manufactures.coll.fetch success', res
          suppliers = new Array
          @suppliers.forEach (supplier) =>
            suppliers.push
              id:   supplier.get('id')
              name: supplier.get('name')

          @$el.html @template
            'create':           lang.translate 'app', 'create'
            'send':             lang.translate 'app', 'send'
            'aggregate':        lang.translate 'archive', 'aggregate'
            'name':             lang.translate 'aggregates', 'name'
            'code':             lang.translate 'aggregates', 'code'
            'sample_weight':    lang.translate 'aggregates', 'sample_weight'
            'pa':               lang.translate 'aggregates', 'pa'
            'prd':              lang.translate 'aggregates', 'prd'
            'pssd':             lang.translate 'aggregates', 'pssd'
            'wa24':             lang.translate 'aggregates', 'wa24'
            'origin':           lang.translate 'aggregates', 'origin'
            'cost':             lang.translate 'aggregates', 'cost'
            'natural':          lang.translate 'aggregates', 'natural'
            'crushed':          lang.translate 'aggregates', 'crushed'
            'grading_analysis': lang.translate 'aggregates', 'grading_analysis'
            'analysis_date':    lang.translate 'aggregates', 'analysis_date'
            'author':           lang.translate 'aggregates', 'author'
            'sieves_series':    lang.translate 'sieves', 'series'
            'supplier':         lang.translate 'aggregates', 'supplier'
            'suppliers':        suppliers

          @collection = new Collection
          @collection.fetch
            data: $.param(id_user: @user.get('id'))
            error: (coll, res) ->
              console.log 'aggregates.collection.fetch -> error', res
            success: (coll, res) =>
              console.log 'aggregates.collection.fetch -> success', res
              @$el.before (new DataTable(@router, @collection,
                title: lang.translate 'aggregates', 'datatable_title'
                sortable: true
                navigable: true
                format: [
                  code: 'name'
                  translate: false
                ,
                  code: 'code'
                  translate: false
                ,
                  code: 'sample_weight'
                  translate: false
                ,
                  code: 'cost'
                  translate: false
                ]
              )).render().el

      return this

    events:
      'change select.sieves_series': 'seriesChanged'
      submit:                       'newAggregate'
    seriesChanged: (e) ->
      console.log 'AggregatesView.seriesChanged'
      current = $(e.currentTarget).val()
      fields = @$el.find 'fieldset.grading_analysis > ol.fields'

      if current is ''
        fields.find('li.sieve').remove()
      else
        if @sieves.length is 0
          @sieves.fetch
            error: (coll, res) ->
              console.log 'sieves fetch error', res
            success: (coll, res) =>
              console.log 'sieves fetch success', res
              fields.find('li.sieve').remove()
              @showSieves()
        else
          fields.find('li.sieve').remove()
          @showSieves()

    showSieves: ->
      fields = @$el.find 'fieldset.grading_analysis > ol.fields'
      template = Handlebars.compile sievesTemplate
      sieves = @sieves.where
        series: fields.find('select.sieves_series').val()

      console.log 'AggregatesView.showSieves', sieves

      sieves.forEach (sieve) =>
        fields.append template
          opening: sieve.get 'opening'
          code:    sieve.get 'id'


    newAggregate: (e) ->
      e.preventDefault()
      fields = @$el.find 'ol.fields > li'

      screenings = new Array
      @$el.find('li.sieve > input').each (item) ->
        screenings.push
          id_sieve: $(this).data('sieve')
          imr:      $(this).val()

      model = new Model
        id_user:       @user.get('id')
        id_supplier:   fields.find('select.supplier').val()
        name:          fields.find('input.name').val()
        code:          fields.find('input.code').val()
        sample_weight: fields.find('input.sample_weight').val()
        pa:            fields.find('input.pa').val()
        prd:           fields.find('input.prd').val()
        pssd:          fields.find('input.pssd').val()
        wa24:          fields.find('input.wa24').val()
        origin:        fields.find('select.origin').val()
        cost:          fields.find('input.cost').val()
        series:        fields.find('select.sieves_series').val()
        analysis_date: fields.find('input.analysis_date').val()
        author:        fields.find('input.author').val()
        screenings:    screenings

      @listenToOnce model, 'sync', =>
        @collection.fetch
          data: $.param id_user: @user.get('id')

      model.save
        error: (model, res) ->
          console.log 'aggregate.save error', res
        success: (model, res) ->
          console.log 'aggregate.save success', res
