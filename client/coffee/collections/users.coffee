define [
  'jquery'
  'underscore'
  'backbone'
  'models/user'
], ($, _, Backbone, User) ->

  class Users extends Backbone.Collection
    url:   '/users'
    model: User
