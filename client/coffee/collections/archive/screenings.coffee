define [
  'jquery'
  'underscore'
  'backbone'
  'models/archive/screening'
], ($, _, Backbone, Screening) ->

  class Screenings extends Backbone.Collection
    url: '/screenings'
    model: Screening
