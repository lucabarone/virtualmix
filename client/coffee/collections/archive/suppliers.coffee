define [
  'jquery'
  'underscore'
  'backbone'
  'models/archive/supplier'
], ($, _, Backbone, Supplier) ->

  class Suppliers extends Backbone.Collection
    url: '/suppliers'
    model: Supplier
