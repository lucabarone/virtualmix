define [
  'jquery'
  'underscore'
  'backbone'
  'models/archive/additive'
], (
  $
  _
  Backbone
  Additive
) ->

  class Additives extends Backbone.Collection
    url: '/additives'
    model: Additive
