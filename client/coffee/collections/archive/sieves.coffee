define [
  'jquery'
  'underscore'
  'backbone'
  'models/archive/sieve'
], ($, _, Backbone, Sieve) ->

  class Sieves extends Backbone.Collection
    url: '/sieves'
    model: Sieve
