define [
  'jquery'
  'underscore'
  'backbone'
  'models/archive/cement'
], ($, _, Backbone, Cement) ->

  class Cements extends Backbone.Collection
    url: '/cements'
    model: Cement
