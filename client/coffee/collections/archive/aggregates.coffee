define [
  'jquery'
  'underscore'
  'backbone'
  'models/archive/aggregate'
  'collections/archive/screenings'
], ($, _, Backbone, Aggregate, Screenings) ->

  class Aggregates extends Backbone.Collection
    url: '/aggregates'
    model: Aggregate

    initialize: ->
      @screenings = new Screenings
