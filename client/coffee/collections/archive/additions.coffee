define [
  'jquery'
  'underscore'
  'backbone'
  'models/archive/addition'
], (
  $
  _
  Backbone
  Addition
) ->

  class Aggregates extends Backbone.Collection
    url: '/additions'
    model: Addition
