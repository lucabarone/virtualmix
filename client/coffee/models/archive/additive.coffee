define [
  'jquery'
  'underscore'
  'backbone'
], (
  $
  _
  Backbone
) ->

  class Additive extends Backbone.Model
    urlRoot: '/additives'
