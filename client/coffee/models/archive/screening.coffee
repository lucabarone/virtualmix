define [
  'jquery'
  'underscore'
  'backbone'
], ($, _, Backbone) ->

  class Screening extends Backbone.Model
    urlRoot: '/screenings'
