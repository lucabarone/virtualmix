define [
  'jquery'
  'underscore'
  'backbone'
], ($, _, Backbone) ->

  class Supplier extends Backbone.Model
    urlRoot: '/suppliers'
