define [
  'jquery'
  'underscore'
  'backbone'
], (
  $
  _
  Backbone
) ->

  class Cement extends Backbone.Model
    urlRoot: '/cements'
