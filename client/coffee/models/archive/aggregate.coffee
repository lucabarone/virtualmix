define [
  'jquery'
  'underscore'
  'backbone'
  'collections/archive/screenings'
], ($, _, Backbone, Screenings) ->

  class Aggregate extends Backbone.Model
    urlRoot: '/aggregates'

    initialize: ->
      @screenings = new Screenings
