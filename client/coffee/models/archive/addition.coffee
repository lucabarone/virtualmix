define [
  'jquery'
  'underscore'
  'backbone'
], (
  $
  _
  Backbone
) ->

  class Addition extends Backbone.Model
    urlRoot: '/additions'
