define [
  'jquery'
  'underscore'
  'backbone'
], ($, _, Backbone) ->

  class Sieve extends Backbone.Model
    urlRoot: '/sieves'
