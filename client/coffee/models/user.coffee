define [
  'jquery'
  'underscore'
  'backbone'
], ($, _, Backbone) ->

  class User extends Backbone.Model
    urlRoot: '/users'
    ping:    undefined

    isLogged: ->
      not @isNew()

    check: (options) ->
      $.ajax
        type:     'GET'
        url:      '/users/check'
        dataType: 'json'

        success: (data) =>
          @set data
          console.log 'check success', data
          @startPing() if data?
          options.success()
          @trigger 'log'
        error: (data) ->
          console.warn 'User.check error ->', data

    login: (options) ->
      console.log 'User.login'
      unless options? or options.email? or options.password?
        console.error 'User.login: invalid options object ->', options
      else
        $.ajax
          type:     'PUT'
          url:      '/users/login'
          dataType: 'json'
          data:     options

          success: (data) =>
            console.log 'User.login success'
            @set data
            @startPing()
            @fetch
              success: =>
                @trigger 'log'
          error: (data) ->
            console.warn 'User.login error ->', data

    logout: ->
      $.ajax
        type: 'PUT'
        url:  '/users/logout'

        success: (data) =>
          console.log 'User.logout success ->', data
          @clear()
          @stopPing()
          @trigger 'log'
        error: (data) ->
          console.log 'User.logout error ->', data

    startPing: ->
      @ping = setInterval (=>
        $.ajax
          type: 'PUT'
          url:  '/users/ping'
      ), 15000 unless @ping?
    stopPing: ->
      if @ping?
        clearInterval @ping
        @ping = undefined
