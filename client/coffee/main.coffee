require.config
  shim:
    'handlebars':
      exports: 'Handlebars'
    'highcharts':
      exports: 'Highcharts'
      deps: ['jquery']

  paths:
    jquery:     'vendor/jquery/jquery'
    underscore: 'vendor/underscore/underscore'
    backbone:   'vendor/backbone/backbone'
    handlebars: 'vendor/handlebars/handlebars'
    text:       'vendor/require/text'
    highcharts: 'vendor/highcharts/highcharts'
    accounting: 'vendor/accounting/accounting'
    templates:  '../templates'

require [
  'jquery'
  'underscore'
  'backbone'
  'router'
  'models/user'
  'highcharts'
], ($, _, Backbone, Router, User, Highcharts) ->

  Highcharts.setOptions
    chart:
      backgroundColor: 'transparent'
      style:
        fontFamily: 'Arial, Helvetica, Verdana, sans-serif'
    colors: ['#3F4C6C']
    title:
      style:
        color:      '#3F4C6C'
        fontWeight: 'bold'
    xAxis:
      minorTickInterval: 'auto'
      lineColor:         'rgba(0, 0, 0, 0.1)'
      lineWidth:         1
      tickColor:         'rgba(0, 0, 0, 0.1)'
      title:
        style:
          color:      '#3F4C6C'
          fontWeight: 'bold'
          fontSize:   '12px'
    tooltip:
      borderWidth:  0
      borderRadius: 2
    legend:
      borderWidth: 0
    credits:
      enabled: false


  user = new User
  router = new Router(user: user)
  unlock = ->
    lock = $('#lock')
    form = lock.find 'form'
    field = form.find 'input.unlock'

    form.fadeOut '300', ->
      lock.fadeOut '300'
      $('body').removeClass 'zoom-out'
      $('#cover').removeClass 'blur'

  $('#logo').on 'click', (e) ->
    e.preventDefault()
    router.goto ''

  $('#lock > form').submit (e) ->
    e.preventDefault()
    if $('#lock input.unlock').val() is 'grom'
      unlock()

  user.once 'log', ->
    unless user.isNew()
      unlock()


  user.check
    success: () ->
      Backbone.history.start pushState: true
