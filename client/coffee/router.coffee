define [
	'jquery'
	'underscore'
	'backbone'
	'lib/lang'
	'views/userPanel'
	'views/mainPanel'
	'views/login/content'
	'views/profile/content'
	'views/archive/content'
	'views/archive/aggregates'
	'views/archive/aggregate'
	'views/archive/cements'
	'views/archive/additives'
	'views/archive/additions'
	'views/archive/suppliers'
], (
	$
	_
	Backbone
	lang
	UserPanel
	MainPanel
	Login
	Profile
	Archive
	Aggregates
	Aggregate
	Cements
	Additives
	Additions
	Suppliers
) ->

	class Router extends Backbone.Router
		initialize: (options) ->
			console.log 'Router.initialize'
			@user = options.user

			@listenToOnce @user, 'log', =>
				@assign (new UserPanel(
					router: this
					user:   @user
				)), '#cover nav.user'

				@add (new MainPanel(
					router: this
					user:   @user
				)), '#content-wrapper nav.main.tile'

		goto: (where) ->
			@navigate where, trigger: true

		assign: (view, selector = '#content') ->
			$(selector).html view.render().el
		add: (view, selector) ->
			$(selector).append view.render().el

		routes:
			'':               'home'
			'login':          'login'
			'profile':        'profile'
			'archive':        'archive'
			'aggregates':     'aggregates'
			'aggregates/:id': 'aggregate'
			'cements':        'cements'
			'additives':      'additives'
			'additions':      'additions'
			'suppliers':      'suppliers'
		home: ->
			template = '<h1>'+ lang.translate('home', 'welcome to')
			template += ' Virtualmix</h1>'
			$('#content').html template

		login: ->
			unless @user.isLogged()
				@assign (new Login(
					router: this
					user: @user
				))
			else
				@goto ''

		profile: ->
			if @user.isLogged()
				@assign (new Profile(this, @user))
			else
				@goto ''

		archive: ->
			if @user.isLogged()
				@assign (new Archive(router: this))
			else
				@goto ''

		aggregates: ->
			if @user.isLogged()
				@assign (new Aggregates(this, @user))
			else
				@goto ''

		aggregate: (id) ->
			if @user.isLogged()
				@assign (new Aggregate(this, @user, id))
			else
				@goto ''

		cements: ->
			if @user.isLogged()
				@assign (new Cements(this, @user))
			else
				@goto ''

		additives: ->
			if @user.isLogged()
				@assign (new Additives(this, @user))
			else
				@goto ''

		additions: ->
			if @user.isLogged()
				@assign (new Additions(this, @user))
			else
				@goto ''

		suppliers: ->
			if @user.isLogged()
				@assign (new Suppliers(this, @user))
			else
				@goto ''
