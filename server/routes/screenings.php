<?php
  use RedBean_Facade as Orm;

  switch ($req->getMethod()) {
    case 'GET':
      $data = $req->get();

      $app->get('/screenings', function () use ($app, $res, $data) {
        if (array_key_exists('id_aggregate', $data)) {
          $id_aggregate = $data['id_aggregate'];
        }

        if (isset($id_aggregate)) {
          $out = array();
          if ($screenings = Orm::find('screenings', 'id_aggregate = ?', array($id_aggregate))) {
            $out = Orm::exportAll($screenings);
          }

          $res['Content-Type'] = 'application/json';
          $res->status(200);
          $res->body(json_encode($out, JSON_NUMERIC_CHECK));
          $app->stop();
        }

        $res->status(400);
        $res->body('access denied');
        $app->stop();
      });
      break;

    default:
      exit('screenings route: no request handler');
      break;
  }
?>
