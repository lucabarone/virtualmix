<?php
  use RedBean_Facade as Orm;

  switch ($req->getMethod()) {
    case 'GET':
      $data = $req->get();

      $app->get('/aggregates/:id', function ($id) use ($app, $res) {
        $aggregate = Orm::findOne('aggregates', 'id = ?', array($id));

        $res['Content-Type'] = 'application/json';
        $res->status(200);
        $res->body(json_encode($aggregate->export(), JSON_NUMERIC_CHECK));
        $app->stop();
      });

      $app->get('/aggregates', function () use ($app, $res, $data) {
        if (array_key_exists('id_user', $data)) {
          $id_user = $data['id_user'];
        }

        if (isset($id_user)) {
          $out = array();
          if ($aggregates = Orm::find('aggregates', 'id_user = ?', array($id_user))) {
            $out = Orm::exportAll($aggregates);
          }

          $res['Content-Type'] = 'application/json';
          $res->status(200);
          $res->body(json_encode($out, JSON_NUMERIC_CHECK));
          $app->stop();
        }

        $res->status(400);
        $res->body('access denied');
        $app->stop();
      });
      break;

    case 'POST':
      $data = json_decode($req->getBody());

      $app->post('/aggregates', function () use ($app, $res, $data) {
        $aggregate = Orm::dispense('aggregates');

        $aggregate->id_user       = $data->id_user;
        $aggregate->id_supplier   = $data->id_supplier;
        $aggregate->name          = $data->name;
        $aggregate->code          = $data->code;
        $aggregate->sample_weight = $data->sample_weight;
        $aggregate->pa            = $data->pa;
        $aggregate->prd           = $data->prd;
        $aggregate->pssd          = $data->pssd;
        $aggregate->wa24          = $data->wa24;
        $aggregate->origin        = $data->origin;
        $aggregate->cost          = $data->cost;
        $aggregate->series        = $data->series;
        $aggregate->author        = $data->author;

        $id = Orm::store($aggregate);

        foreach ($data->screenings as $screening) {
          $current = Orm::dispense('screenings');

          $current->id_user      = $data->id_user;
          $current->id_aggregate = $id;
          $current->id_sieve     = $screening->id_sieve;
          $current->date         = $data->analysis_date;
          $current->imr          = $screening->imr;

          Orm::store($current);
        }

        $res['Content-Type'] = 'application/json';
        $res->status(200);
        $res->body(json_encode($id, JSON_NUMERIC_CHECK));
        $app->stop();
      });
      break;

    case 'DELETE':
      $app->delete('/aggregates/:id', function ($id) use ($app, $res) {
        if ($user = Orm::findOne('users', 'session = ?', array(session_id()))) {
          if ($aggregate = Orm::findOne('aggregates', 'id = :id AND id_user = :id_user', array(
            ':id'      => $id,
            ':id_user' => $user->id
          ))) {
            Orm::trash($aggregate);

            if ($screenings = Orm::find('screenings', 'id_user = :id_user AND id_aggregate = :id_aggregate', array(
              ':id_user'      => $user->id,
              ':id_aggregate' => $id
            ))) {
              Orm::trashAll($screenings);
            }

            $res['Content-Type'] = 'application/json';
            $res->status(200);
            $res->body(json_encode(true, JSON_NUMERIC_CHECK));
            $app->stop();
          }
        }


        $res->status(400);
        $res->body("DELETE /aggregates/$id failed.");
        $app->stop();
      });
      break;

    default:
      exit('aggregates route: no request handler');
      break;
  }
?>
