<?php
  use RedBean_Facade as Orm;

  switch ($req->getMethod()) {
    case 'GET':
      $app->get('/sieves', function () use ($app, $res) {
        $sieves = Orm::exportAll(Orm::find('sieves'));

        $res['Content-Type'] = 'application/json';
        $res->status(200);
        $res->body(json_encode($sieves, JSON_NUMERIC_CHECK));
        $app->stop();
      });
      break;

    default:
      exit('sieves route: no request handler');
      break;
  }
?>
