<?php
  use RedBean_Facade as Orm;

  switch ($req->getMethod()) {
    case 'GET':
      $data = $req->get();

      $app->get('/cements', function () use ($app, $res, $data) {
        if (array_key_exists('id_user', $data)) {
          $id_user = $data['id_user'];
        }

        if (isset($id_user)) {
          $out = array();
          if ($cements = Orm::find('cements', 'id_user = ?', array($id_user))) {
            $out = Orm::exportAll($cements);
          }

          $res['Content-Type'] = 'application/json';
          $res->status(200);
          $res->body(json_encode($out, JSON_NUMERIC_CHECK));
          $app->stop();
        }

        $res->status(400);
        $res->body('access denied');
        $app->stop();
      });
      break;

    case 'POST':
      $data = json_decode($req->getBody());

      $app->post('/cements', function () use ($app, $res, $data) {
        $cement = Orm::dispense('cements');

        $cement->id_user = $data->id_user;
        $cement->id_supplier = $data->id_supplier;
        $cement->name = $data->name;
        $cement->type = $data->type;
        $cement->cost = $data->cost;

        Orm::store($cement);
      });
      break;

    default:
      exit('cements route: no request handler');
      break;
  }
?>
