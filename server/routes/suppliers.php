<?php
  use RedBean_Facade as Orm;

  switch ($req->getMethod()) {
    case 'GET':
      $data = $req->get();

      $app->get('/suppliers', function () use ($app, $res, $data) {
        if (array_key_exists('id_user', $data)) {
          $id_user = $data['id_user'];
        }

        if (array_key_exists('type', $data)) {
          $type = $data['type'];
        }

        if (isset($id_user, $type)) {
          $out = array();
          if ($suppliers = Orm::find('suppliers', 'id_user = :id_user AND type = :type', array(
            ':id_user' => $id_user,
            ':type'    => $type
          ))) {
            $out = Orm::exportAll($suppliers);
          }

          $res['Content-Type'] = 'application/json';
          $res->status(200);
          $res->body(json_encode($out, JSON_NUMERIC_CHECK));
          $app->stop();
        }

        if (isset($id_user)) {
          $out = array();
          if ($suppliers = Orm::find('suppliers', 'id_user = ?', array($id_user))) {
            $out = Orm::exportAll($suppliers);
          }

          $res['Content-Type'] = 'application/json';
          $res->status(200);
          $res->body(json_encode($out, JSON_NUMERIC_CHECK));
          $app->stop();
        }

        $res->status(400);
        $res->body('access denied');
        $app->stop();
      });
      break;

    case 'POST':
      $data = json_decode($req->getBody());

      $app->post('/suppliers', function () use ($app, $res, $data) {
        $supplier = Orm::dispense('suppliers');

        $supplier->name        = $data->name;
        $supplier->city        = $data->city;
        $supplier->region      = $data->region;
        $supplier->address     = $data->address;
        $supplier->postal_code = $data->postal_code;
        $supplier->type        = $data->type;
        $supplier->id_user     = $data->id_user;

        $id = Orm::store($supplier);

        $res['Content-Type'] = 'application/json';
        $res->status(200);
        $res->body(json_encode($id, JSON_NUMERIC_CHECK));
        $app->stop();
      });
      break;

    default:
      exit('suppliers route: no request handler');
      break;
  }
?>
