<?php
	use RedBean_Facade as Orm;

	switch ($req->getMethod()) {
		case 'GET':
			/*
			* Route che si occupa di verificare se l'utente è già loggato
			* o meno, basandosi sulla session e sull'ultimo ping.
			* Il ping è impostato a 30 secondi: se entro tale lasso di tempo
			* non viene rinnovato, l'utente viene considerato come non loggato.
			*/
			$app->get('/users/check', function () use ($app, $res) {
				$out = null;

				if ($user = Orm::findOne('users', 'session = ?', array(session_id()))) {
					$diff = time() - strtotime($user->ping);
					if ($diff <= 30) {
						$out['id']      = $user->id;
						$out['email']   = $user->email;
						$out['name']    = $user->name;
						$out['surname'] = $user->surname;
						$out['role']    = $user->role;
						$out['type']    = $user->type;
					} else {
						$user->session = null;
						$user->ping    = new DateTime('2000-01-01 20:00:00');
						Orm::store($user);
					}
				}

				$res['Content-Type'] = 'application/json';
				$res->status(200);
				$res->body(json_encode($out, JSON_NUMERIC_CHECK));
				$app->stop();
			});

			/*
			* Route che fornisce i dati dell'utente richiesto
			*/
			$app->get('/users/:id', function ($id) use ($app, $res) {
				$out = array();

				if ($user = Orm::findOne('users', 'id = ?', array($id))) {
					$out['id']      = $user->id;
					$out['email']   = $user->email;
					$out['name']    = $user->name;
					$out['surname'] = $user->surname;
					$out['role']    = $user->role;
					$out['type']    = $user->type;
				}

				$res['Content-Type'] = 'application/json';
				$res->status(200);
				$res->body(json_encode($out, JSON_NUMERIC_CHECK));
				$app->stop();
			});

			$app->get('/users', function () use ($app, $res) {
				$users = Orm::getAll('SELECT id, email, name, surname, role, type FROM users');

				$res['Content-Type'] = 'application/json';
				$res->status(200);
				$res->body(json_encode($users, JSON_NUMERIC_CHECK));
				$app->stop();
			});
			break;

		case 'PUT':
			$data  = $req->put();
			$login = function ($user) use ($app, $res) {
				$user->session = session_id();
				$user->ping    = new DateTime();
				Orm::store($user);

				$res['Content-Type'] = 'application/json';
				$res->status(200);
				$res->body(json_encode(array('id' => $user->id), JSON_NUMERIC_CHECK));
				$app->stop();
			};

			/*
			* Route dedicata al login dell'utente, si occupa di
			* aggiornare i campi session e ping, se le credenziali
			* sono corrette, e se l'utente non è già loggato su un'altra
			* macchina.
			*/
			$app->put('/users/login', function () use ($app, $res, $data, $decrypt, $login) {
				if ($user = Orm::findOne('users', 'email = ?', array($data['email']))) {
					if ($user->password == $decrypt($user->password, $data['password'])) {
						if (is_null($user->session) or $user->session == session_id()) {
							$login($user);
						} else {
							$diff = time() - strtotime($user->ping);
							if ($diff > 30) {
								// exit(json_encode($diff));
								$login($user);
							} else {
								$res->status(400);
								$res->body('account already in use');
								$app->stop();
							}
						}
					}
				}
				$res->status(400);
				$res->body('wrong email or password');
				$app->stop();
			});

			/*
			* Route dedicata al rinnovo della connessione dell'utente,
			* si occupa appunto di aggiornare il campo ping,
			* con il DateTime corrente, in modo tale da mantenere
			* attiva la connessione.
			*/
			$app->put('/users/ping', function () use ($app, $res) {
				if ($user = Orm::findOne('users', 'session = ?', array(session_id()))) {
					$user->ping = new DateTime();
					Orm::store($user);

					$res['Content-Type'] = 'application/json';
					$res->status(200);
					$res->body(json_encode(true, JSON_NUMERIC_CHECK));
					$app->stop();
				}

				$res->status(400);
				$res->body('session expired');
				$app->stop();
			});

			/*
			* Route dedicata al logout dell'utente, si occupa di
			* aggiornare i campi session e ping annullandoli.
			*/
			$app->put('/users/logout', function () use ($app, $res) {
				if ($user = Orm::findOne('users', 'session = ?', array(session_id()))) {
					$user->session = null;
					$user->ping    = new DateTime('2000-01-01 20:00:00');
					Orm::store($user);
				}

				$res['Content-Type'] = 'application/json';
				$res->status(200);
				$res->body(json_encode(true, JSON_NUMERIC_CHECK));
				$app->stop();
			});
			break;

		default:
			exit('users route: no request handler');
			break;
	}
?>
