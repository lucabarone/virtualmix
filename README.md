# VIRTUALMIX PROJECT

## Sistemi operativi supportati al momento
- Linux/Ubuntu-like

## Pre-requisiti
+ [Git](http://git-scm.com/) come sistema di versionamento
+ [Composer](http://getcomposer.org/) come gestore di pacchetti per php
+ [Node.js](http://nodejs.org) per utilizzare npm come gestori di pacchetti javascript
+ [coffeescript](http://coffeescript.org/) installato globalmente tramite npm: `sudo npm install -g coffee-script`
+ [stylus](http://learnboost.github.io/stylus/) installato globalmente tramite npm: `sudo npm install -g stylus`
***

## Installazione rapida
Clonarsi il repository da questa pagina,
Da terminale:
```
#!bash
cd virtualmix/
composer install

cd client/
coffee -o js/ -bc coffee/
stylus -c stylus/style.styl -o css/
```
A questo punto dovresti essere pronto per testare l'applicazione sul tuo browser.