<?php
  require 'vendor/autoload.php';

  use RedBean_Facade as Orm;

  session_start();

  /** INIZIALIZZAZIONE VARIABILI
  * $app - oggetto principale del framework slim;
  * $req - oggetto raprresentante la richiesta del client;
  * $res - oggetto rappresentante la risposta al client;
  * $crypt, $encrypt, $decrypt - funzioni dedicate al criptaggio delle password;
  */
  $app = new \Slim\Slim(array(
    'templates.path' => 'client/',
    'mode'           => 'development'
    // 'mode'           => 'database'
  ));
  $req   = $app->request();
  $res   = $app->response();
  $crypt = function ($salt, $password) {
    $out = $salt .$password;
    for ($i=0; $i < 100000; $i++) {
      $out = hash('sha256', $out);
    }
    return $salt . $out;
  };
  $encrypt = function ($email, $password) use ($crypt) {
    $salt = hash('sha256', uniqid(mt_rand(), true) .'starlight'. strtolower($email));
    return $crypt($salt, $password);
  };
  $decrypt = function ($hashedPassword, $password) use ($crypt) {
    $salt = substr($hashedPassword, 0, 64);
    return $crypt($salt, $password);
  };

  /** CONFIGURAZIONE
  * In questa sezione vengono configurati i principali strumenti utilizzati
  * nel resto del codice.
  */
  $res['X-Powered-By'] = 'Virtualmix';

  if ($req->getIp() == '127.0.0.1') {
    Orm::setup('mysql:host=localhost;dbname=virtualmix', 'root', 'admin');
  } else {
    Orm::setup('mysql:host=hostingmysql227.register.it;dbname=virtualmix_it_dev', 'SC17632_master', 'bernadelli');
  }

  $app->configureMode('database', function () use ($app, $encrypt) {
    Orm::nuke();

    $users = json_decode(file_get_contents('./config/users.json'));
    foreach ($users as $user) {
      $current = Orm::dispense('users');

      $current->email    = $user->email;
      $current->password = $encrypt($user->email, $user->password);
      $current->name     = $user->name;
      $current->surname  = $user->surname;
      $current->role     = $user->role;
      $current->type     = $user->type;
      $current->session  = null;
      $current->ping     = new DateTime('2000-01-01 20:00:00');

      Orm::store($current);
    }

    $sieves = json_decode(file_get_contents('./config/sieves.json'));
    foreach ($sieves as $sieve) {
      $current = Orm::dispense('sieves');

      $current->series  = $sieve->series;
      $current->opening = $sieve->opening;

      Orm::store($current);
    }

    exit('database populated');
  });

  /** ROUTER
  * Questa sezione è dedicata allo smistamento delle richieste: viene effettuata
  * una prima scrematura verificando che le richieste siano AJAX o meno.
  */
  if ($req->isAjax()) {

    // Estraggo il soggetto della richiesta
    $subject = array_filter(explode('/', $req->getPath()));
    $subject = $subject[1];

    // Richiedo il file dedicato alla gestione delle route corrispondenti al soggetto.
    require_once "server/routes/$subject.php";
  } else {

    $renderClient = function () use ($app) {
      $app->render('main.html');
    };

    $app->get('/', $renderClient);
    $app->get('/:all+', $renderClient);
  }

  $app->run();
?>
